<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('adminlogin','APIController@adminlogin');
Route::post('login','APIController@login');
Route::post('register','APIController@register');
Route::post('getUsers','APIController@getUsers');
Route::post('changePassword','APIController@changePassword');
Route::post('forgotPassword','APIController@forgotPassword');
Route::post('verifyEmail','APIController@verifyOtp');
Route::post('resetPassword','APIController@changePassword');
Route::get('getProfile', 'APIController@getProfile');
Route::post('addCategory', 'APIController@addCategory');
Route::post('getCategory', 'APIController@getCategory');
Route::post('editProfile', 'APIController@editProfile');
Route::post('homelisting', 'APIController@homelisting');
Route::post('addAsset', 'APIController@addPost');
Route::post('editAsset', 'APIController@editAsset');
Route::post('getAssets', 'APIController@getAssets');
Route::post('visibilityList', 'APIController@visibilityList');
Route::post('addAssetToTimeline', 'APIController@addAssetToTimeline');
Route::post('search', 'APIController@search');
Route::post('addRole', 'APIController@addRole');
Route::post('getRoles', 'APIController@getRoles');
Route::post('addPermissions', 'APIController@addPermissions');
Route::post('getPermissions', 'APIController@getPermissions');
Route::post('assignRole', 'APIController@assignRole');
Route::post('setRatio', 'APIController@setRatio');
Route::post('getFinancials', 'APIController@getFinancials');
Route::post('addContent', 'APIController@addContent');
Route::post('getContent', 'APIController@getContent');
Route::post('addPost', 'APIController@addPost');
