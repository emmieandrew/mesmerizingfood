<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Admin;
use App\Models\Category;
use App\Models\Permission;
use App\Models\Role;
use App\Models\Financial;
use App\Models\Content;
use App\Models\Detail;
use App\Models\Post;
use Validator;
use Illuminate\Support\Facades\Hash;
use DB;

class APIController extends Controller
{
    public $unauthorised = 401;
    public $successStatus = 200;
    public $badrequest = 400;


    /**************************************************************************/


    public function register(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'dob' => 'required',
            'country' => 'required',
            'city' => 'required',
            'password' => 'required',
            'gender' => 'required',
            'state' => 'required',
            'type' => 'required', // 1 for totor and 2 for learner
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 0,'message'=>"All fields are required"]);
        }

        $checkEmail = User::where(['email'=>$request->email])->first();

        if($checkEmail){
             return response()->json(['status' => 0,'message'=>'Email already exist.']);
        }

        $data = array(
            'type'=>$request->type,
            'name'=>$request->name,
            'email'=>$request->email,
            'phone'=>$request->phone,
            'dob'=>$request->dob,
            'city'=>$request->city,
            'country'=>$request->country,
            'gender'=>$request->gender,
            'state'=>$request->state,
            'password'=>Hash::make($request->password)
        );

        // dd($data);
        $users = User::create($data);
        
        // print_r($users);die();
        $detail = array(
            'user_id'=>$users->id
        );
        $userdetails = Detail::create($detail);
        // $url = url('public/images');
        $user = User::where(['id'=>$users->id])->first();

        // $data['id'] =  $users->id;


        return response()->json(['status' => 1,'message' => 'Registered Successfully.','result' =>$user]);

    }

/**************************************************************************/

    public function login(Request $request){

        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 0,'message'=>"All fields are required"]);
        }
        // $url = url('public/images');
        $checkEmail = User::where(['email'=>$request->email])->first();
        if($checkEmail){

            if (Hash::check($request->password, $checkEmail->password))
            {
                return response()->json(['status' => 1,'message' => 'Login Successfully.','result' => $checkEmail]);
            }else{
                return response()->json(['status' => 0,'message'=>'Password Mismatched']);
            }
        }else{
            return response()->json(['status' => 0,'message'=>'Account Not Registered or Deactivated Please contact support for more information.']);
        }


    }

    public function adminlogin(Request $request){

        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 0,'message'=>"All fields are required"]);
        }
        // $url = url('public/images');
        $checkEmail = Admin::where(['email'=>$request->email])->first();
        if($checkEmail){

            if (Hash::check($request->password, $checkEmail->password))
            {
                return response()->json(['status' => 1,'message' => 'Login Successfully.','result' => $checkEmail]);
            }else{
                return response()->json(['status' => 0,'message'=>'Password Mismatched']);
            }
        }else{
            return response()->json(['status' => 0,'message'=>'Account Not Registered or Deactivated Please contact support for more information.']);
        }


    }


    public function getUsers(Request $request){
        $validator = Validator::make($request->all(), [
            'type' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 0,'message'=>"All fields are required"]);
        }else{
            if ($request->type == 1) {
                $userArr = [];
                $allUsers = User::orderBy('id','DESC')
                ->where('type',1)
                ->get();
                $larr = json_decode($allUsers,true);
                foreach($larr AS $user){
                    $list['id'] = (String)$user['id'];
                    $list['name'] = $user['name'];
                    $list['email'] = $user['email'];
                    $list['dob'] = $user['dob'];
                    $list['type'] = $user['type'];
                    $list['status'] = $user['status'];
                    $list['phone'] = $user['phone'];
                    $list['country'] = $user['country'];
                    $list['city'] = $user['city'];
                    $list['image'] = url('/').'/user_images/'.$user['image'];
                    $userArr[] = $list;
                }
                return response()->json([
                    'status'=>'1',
                    'message'=>'Users fetched successfully',
                    'userArray'=>$userArr
                ], $this->successStatus);
            }elseif ($request->type == 2) {
                $userArr = [];
                $allUsers = User::orderBy('id','DESC')
                ->where('type',2)
                ->get();
                $larr = json_decode($allUsers,true);
                foreach($larr AS $user){
                    $list['id'] = (String)$user['id'];
                    $list['name'] = $user['name'];
                    $list['email'] = $user['email'];
                    $list['dob'] = $user['dob'];
                    $list['type'] = $user['type'];
                    $list['status'] = $user['status'];
                    $list['phone'] = $user['phone'];
                    $list['country'] = $user['country'];
                    $list['city'] = $user['city'];
                    $list['image'] = url('/').'/public/images/user_images/'.$user['image'];
                    $userArr[] = $list;
                }
                return response()->json([
                    'status'=>'1',
                    'message'=>'Users fetched successfully',
                    'userArray'=>$userArr
                ], $this->successStatus);
            }
        }
    }
/**************************************************************************/


    public function changePassword(Request $request){

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'password' => 'required',
            'cpassword' => 'required',

        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 0,'message'=>"All fields are required"]);
        }

        $user = User::where(['id'=>$request->user_id])->first();

        if(!$user){
            return response()->json(['status' => 0,'message'=>'Invalid user id']);
        }

        if ($request->password  != $request->cpassword)
        {
            return response()->json(['status' => 0,'message'=>'Password Mismatch']);
        }

        User::where('id', $request->user_id)->update([
           'password' => Hash::make($request->password),
           // 'user_pass' => $request->password,
        ]);

        return response()->json(['status' => 1,'message' => 'Changed Successfully.']);


    }


/**************************************************************************/


    public function forgotPassword(Request $request){

        $validator = Validator::make($request->all(), [
            // 'user_id' => 'required',
            'email' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 0,'message'=>"All fields are required"]);
        }

        $user = User::where(['email'=>$request->email])->first();

        if(!$user){
            return response()->json(['status' => 0,'message'=>'Invalid user email']);
        }


        $otp  = rand(1111,9999);

        User::where('email', $request->email)->update([
           'otp' => $otp
        ]);

       	$result = ['otp'=>$otp];

       return response()->json(['status' => 1,'message' => 'OTP sent to your email.','result' => $result]);


    }


/**************************************************************************/


    public function verifyOtp(Request $request){

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'otp' => 'required',

        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 0,'message'=>"All fields are required"]);
        }

        $user = User::where(['id'=>$request->user_id,'otp'=>$request->otp])->first();

        if(!$user){
            return response()->json(['status' => 0,'message'=>'Invalid OTP']);
        }

        // User::where('id', $request->user_id)->update([
        //    'is_active' => 2
        // ]);

        return response()->json(['status' => 1,'message' => 'Verified Successfully.']);

    }

/**************************************************************************/

    public function editProfile(Request $request){

         $validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 0,'message'=>"All fields are required"]);
        }

        $user = User::where(['id'=>$request->user_id])->first();
        // print_r(json_encode($user));die();
        if(!$user){
            return response()->json(['status' => 0,'message'=>'Invalid user id']);
        }

        if ($request->hasFile('image')) {

            if ($request->file('image')->isValid()) {

                $image_name = time().'.'.$request->image->extension();

                $request->image->move(public_path('user_images'), $image_name);

            }else{
                $image_name = $user->image;
            }

        }else{
            $image_name = $user->image;
        }


        $data = array(
        	'name'=>$request->name?$request->name:$user['name'],
        	'phone'=>$request->phone?$request->phone:$user['phone'],
            'country'=>$request->country?$request->country:$user['country'],
            'city'=>$request->city?$request->city:$user['city'],
            'gender'=>$request->gender?$request->gender:$user['gender'],
            'state'=>$request->state?$request->state:$user['state'],
            'email'=>$request->email?$request->email:$user['email'],
            'image'=>$image_name?$image_name:'default.jpg',
        );
        User::where('id', $request->user_id)->update($data);
        
        $userdetail = Detail::where('user_id', $request->user_id)->first();
        $data = [];
        if ($request->hasFile('documents')) {
            foreach($request->file('documents') as $key=>$file){
                $name = time().$file->getClientOriginalName();
                $file->move(public_path('/tutor_documents') . '/', $name);
                $data[$key] = $name;
            }
            $imgs = implode(",", $data);
        }else{
            $imgs = $userdetail->documents;
        }
        $details = array(
            'specification'=>$request->specification?$request->specification:$userdetail['specification'],
        	'tags'=>$request->tags?$request->tags:$userdetail['tags'],
            'languages'=>$request->languages?$request->languages:$userdetail['languages'],
            'experiences'=>$request->experiences?$request->experiences:$userdetail['experiences'],
            'documents'=>$imgs,
        );

        Detail::where('user_id', $request->user_id)->update($details);

        return response()->json(['status' => 1,'message' => 'Updated Successfully.']);

    }

    public function addPost(Request $request){
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'user_id' => 'required',
            'description' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'false'], $this->badrequest);
        }else{
            $data = [];
            if ($request->hasFile('files')) {
                foreach($request->file('files') as $key=>$file){
                    $name = time().$file->getClientOriginalName();
                    $file->move(public_path('/tutor_posts') . '/', $name);
                    $data[$key] = $name;
                }
                $imgs = implode(",", $data);
            }else{
                $imgs = '';
            }
            $data = array(
                'files'=>$imgs,
                'title'=>$request->title,
                'user_id'=>$request->user_id,
                'description'=>$request->description,
            );
            $create = Post::create($data);
            return response()->json(['status' => 1,'message' => 'Post Added Successfully.','result' =>$create]);
        }
    }

    public function getProfile(Request $request){

         $validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 0,'message'=>"All fields are required"]);
        }


        $url = url('/user_images');

        $user = User::where(['id'=>$request->user_id])->select(array('*', DB::raw("CONCAT('$url/', image) AS image")))->first();
        $user['details'] = Detail::where(['user_id'=>$request->user_id])->first();
        $tagmedia = explode(",", $user['details']['documents']);
        $tutorDocArray = [];
        foreach ($tagmedia as $tagmed) {
            $tmedia = asset('/tutor_documents/' . $tagmed);
            $tmedi['media'] = $tmedia;
            $tutorDocArray[] = $tmedi;
        }
        $user['details']['tutor_documents'] = $tutorDocArray;

        $posts = Post::where(['user_id'=>$request->user_id])->get();
        $postArray = [];
        foreach ($posts as $post) {
            $tagmedia = explode(",", $post['files']);
            $mediaArray = [];
            foreach ($tagmedia as $postmed) {
                $psmedia = asset('/tutor_posts/' . $postmed);
                $psmed['media'] = $psmedia;
                $mediaArray[] = $psmed;
            }
            $med['id'] = (String)$post['id'];
            $med['title'] = $post['title'];
            $med['description'] = $post['description'];
            $med['media'] = $mediaArray;
            $postArray[] = $med;
        }
        $user['posts'] = $postArray;
        if(!$user){
            return response()->json(['status' => 0,'message'=>'Invalid user id']);
        }

        return response()->json(['status' => 1,'message' => 'Success','result'=>$user]);

    }

    public function addCategory(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'false'], $this->badrequest);
        }else{
            if ($request->parent_id) {
                $data = array(
                    'type'=>$request->type,
                    'name'=>$request->name,
                    'parent_id'=>$request->parent_id,
                );
                $subcat = Category::create($data);
                return response()->json(['status' => 1,'message' => 'Subcategory Added Successfully.','result' =>$subcat]);
            }else{ // add category
                $data = array(
                    'type'=>$request->type,
                    'name'=>$request->name,
                    'parent_id'=> '0',
                );
                $cat = Category::create($data);
                return response()->json(['status' => 1,'message' => 'Category Added Successfully.','result' =>$cat]);
            }
        }
    }

    public function getCategory(Request $request){
    	$validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'type' => 'required', // BIFURCATION', 'CUISINES', 'MinuteWise
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'false'], $this->badrequest);
        }else{
            // if ($request->type == 'cuisines') {
            //     if ($request->parent_id == '') {
            //         $addArr = [];
            //         $addresses = Category::where('parent_id',0)
            //         ->orderBy('id','DESC')
            //         ->get();
            //         $larr = json_decode($addresses,true);
            //         foreach($larr AS $add){
            //             $list['id'] = (String)$add['id'];
            //             $list['name'] = $add['name'];
            //             $list['parent_id'] = $add['parent_id'];
            //             $addArr[] = $list;
            //         }
            //         return response()->json([
            //             'status'=>'1',
            //             'message'=>'Categories fetched successfully',
            //             'cartArray'=>$addArr
            //         ], $this->successStatus);
            //         }
            // }
            if ($request->parent_id == '') {
                $addArr = [];
                $addresses = Category::where('parent_id',0)
                ->where('type',$request->type)
                ->orderBy('id','DESC')
                ->get();
                $larr = json_decode($addresses,true);
                foreach($larr AS $add){
                    $list['id'] = (String)$add['id'];
                    $list['name'] = $add['name'];
                    $list['parent_id'] = $add['parent_id'];
                    $addArr[] = $list;
                }
                return response()->json([
                    'status'=>'1',
                    'message'=>'Categories fetched successfully',
                    'cartArray'=>$addArr
                ], $this->successStatus);
            }else{
                $addArr = [];
                $addresses = Category::where('parent_id',$request->parent_id)
                ->where('type',$request->type)
                ->orderBy('id','DESC')
                ->get();
                $larr = json_decode($addresses,true);
                foreach($larr AS $add){
                    $list['id'] = (String)$add['id'];
                    $list['name'] = $add['name'];
                    $list['parent_id'] = $add['parent_id'];
                    $addArr[] = $list;
                }
                return response()->json([
                    'status'=>'1',
                    'message'=>'Categories fetched successfully',
                    'subcatArray'=>$addArr
                ], $this->successStatus);
            }
        }
    }

    public function addRole(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'permissions_ids' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'false'], $this->badrequest);
        }else{
            $data = array(
                'role_name'=>$request->name,
                'permissions_ids'=>$request->permissions_ids,
            );
            $role = Role::create($data);
            return response()->json(['status' => 1,'message' => 'Role Added Successfully.','result' =>$role]);
        }
    }

    public function getRoles(Request $request){
        $validator = Validator::make($request->all(), [
            // 'name' => 'required',
            // 'permissions_ids' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'false'], $this->badrequest);
        }else{
            $addArr = [];
            $addresses = Role::orderBy('id','ASC')
            ->get();
            $larr = json_decode($addresses,true);
            foreach($larr AS $add){
                $permisionID = explode(",", $add['permissions_ids']);
                $permissions = Permission::whereIn('id',$permisionID)->get();
                // print_r($permisionID);die();
                $permissionArr = [];
                foreach ($permissions as $perm) {
                    $per['id'] = (String)$perm['id'];
                    $per['name'] = $perm['name'];
                    $permissionArr[] = $per;
                }
                $list['id'] = (String)$add['id'];
                $list['role_name'] = $add['role_name'];
                $list['permissions_ids'] = $add['permissions_ids'];
                $list['permissions'] = $permissionArr;
                $addArr[] = $list;
            }
            return response()->json([
                'status'=>'1',
                'message'=>'Roles fetched successfully',
                'fnancialArray'=>$addArr
            ], $this->successStatus);
        }
    }

    public function addPermissions(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'false'], $this->badrequest);
        }else{
            $data = array(
                'name'=>$request->name,
            );
            $role = Permission::create($data);
            return response()->json(['status' => 1,'message' => 'Permission Added Successfully.','result' =>$role]);
        }
    }

    public function getPermissions(Request $request){
        $validator = Validator::make($request->all(), [
            // 'name' => 'required',
            // 'permissions_ids' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'false'], $this->badrequest);
        }else{
            $addArr = [];
            $addresses = Permission::orderBy('id','ASC')
            ->get();
            $larr = json_decode($addresses,true);
            foreach($larr AS $add){
                $list['id'] = (String)$add['id'];
                $list['name'] = $add['name'];
                $addArr[] = $list;
            }
            return response()->json([
                'status'=>'1',
                'message'=>'Permissions fetched successfully',
                'permissionsArray'=>$addArr
            ], $this->successStatus);
        }
    }

    public function assignRole(Request $request){
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'role_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'false'], $this->badrequest);
        }else{
            $user = User::where('id', $request->user_id)
            ->update([
                'role_id' => $request->role_id,
            ]);
            return response()->json(['status' => 1,'message' => 'Role Added Successfully.']);
        }
    }

    public function setRatio(Request $request){
        $validator = Validator::make($request->all(), [
            'ratio' => 'required',
            'company_id' => 'required',
            'tutor_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'false'], $this->badrequest);
        }else{
            $checkRecord = Financial::where('company_id',$request->company_id)
            ->where('tutor_id',$request->tutor_id)
            ->first();
            if ($checkRecord) {
                $user = Financial::where('company_id',$request->company_id)
                ->where('tutor_id',$request->tutor_id)
                ->update([
                    'ratio' => $request->ratio,
                ]);
                return response()->json(['status' => 1,'message' => 'Ratio Updated Successfully.']);
            }else{
                $data = array(
                    'company_id'=>$request->company_id,
                    'ratio'=>$request->ratio,
                    'tutor_id'=>$request->tutor_id,
                );
                $role = Financial::create($data);
                return response()->json(['status' => 1,'message' => 'Ratio Added Successfully.','result' =>$role]);
            }
        }
    }

    public function getFinancials(Request $request){
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'false'], $this->badrequest);
        }else{
            $addArr = [];
            $addresses = Financial::orderBy('id','ASC')
            ->get();
            $larr = json_decode($addresses,true);
            foreach($larr AS $add){
                $companyname = User::where('id',$add['company_id'])->first();
                $tutorname = User::where('id',$add['tutor_id'])->first();
                $list['id'] = (String)$add['id'];
                $list['company_id'] = $add['company_id'];
                $list['company_name'] = $companyname['name'];
                $list['tutor_id'] = $add['tutor_id'];
                $list['tutor_name'] = $tutorname['name'];
                $list['ratio'] = $add['ratio'];
                $addArr[] = $list;
            }
            return response()->json([
                'status'=>'1',
                'message'=>'Financial fetched successfully',
                'fnancialArray'=>$addArr
            ], $this->successStatus);
        }
    }

    public function addContent(Request $request){
        $validator = Validator::make($request->all(), [
            // 'title' => 'required',
            'content' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'false'], $this->badrequest);
        }else{
            if ($request->id) {
                $user = Content::where('id',$request->id)
                ->update([
                    'content' => $request->content,
                ]);
                return response()->json(['status' => 1,'message' => 'Content Updated Successfully.']);
            }else{
                $data = array('content'=>$request->content);
                $content = Content::create($data);
                return response()->json(['status' => 1,'message' => 'Content Added Successfully.','result' =>$content]);
            }
        }
    }

    public function getContent(Request $request){
        $validator = Validator::make($request->all(), [
            // 'title' => 'required',
            'content_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'false'], $this->badrequest);
        }else{
            $content = Content::where('id',$request->content_id)->first();
            return response()->json(['status' => 1,'message' => 'Content Fetched Successfully.','content' =>$content]);
        }
    }

    // public function addPost(Request $request){

    //     $validator = Validator::make($request->all(), [
    //     	'user_id' => 'required',
    //         'post_name' => 'required',
    //         'post_title' => 'required',
    //         'post_content' => 'required',
    //         'category' => 'required'
    //     ]);

    //     if ($validator->fails()) {
    //         return response()->json(['status' => 0,'message'=>"All fields are required"]);
    //     }


    //     if ($request->hasFile('picture')) {

    //         if ($request->file('picture')->isValid()) {

    //         	$image_name = time().'.'.$request->picture->extension();

    //             $request->picture->move(public_path('asset_images'), $image_name);

    //         }else{
    //             $image_name ="default.png";
    //         }

    //     }else{
    //         $image_name ="default.png";
    //     }


    //     $data = array(
    //     	'post_author'=>$request->user_id,
    //     	'post_title'=>$request->post_title,
    //         'post_content'=>$request->post_content,
    //         'post_status'=>'publish',
    //         'comment_status'=>'open',
    //         'post_excerpt'=>'NULL',
    //         'to_ping'=>'NULL',
    //         'pinged'=>'NULL',
    //         'post_content_filtered'=>'NULL',
    //         'post_name'=>$request->post_title,
    //         'menu_order'=>$request->category
    //     );


    //     $posts = Category::create($data);
    //     // $url = url('public/images');
    //     $post = Post::where(['id'=>$posts->id])->first();


    //     return response()->json(['status' => 1,'message' => 'Post Created Successfully.','result' =>$post]);

    // }

    // public function editAsset(Request $request){

    //      $validator = Validator::make($request->all(), [
    //         'user_id' => 'required',
    //         'asset_id' => 'required'
    //     ]);

    //     if ($validator->fails()) {
    //         return response()->json(['status' => 0,'message'=>"All fields are required"]);
    //     }

    //     $user = User::where(['id'=>$request->user_id])->first();
    //     // print_r(json_encode($user));die();
    //     if(!$user){
    //         return response()->json(['status' => 0,'message'=>'Invalid user id']);
    //     }

    //     if ($request->hasFile('image')) {

    //         if ($request->file('image')->isValid()) {

    //            /* $validated = $request->validate([

    //                 'image' => 'mimes:jpeg,png|max:1014',

    //             ]);*/

    //             //$extension = $request->image->extension();
    //             //$image_name = time().".".$extension;
    //             //$request->image->storeAs('/images', $image_name);

    //             $image_name = time().'.'.$request->image->extension();

    //             $request->image->move(public_path('user_images'), $image_name);

    //         }else{
    //             $image_name = $user->image;
    //         }

    //     }else{
    //         $image_name = $user->image;
    //     }
    //     $post = Post::where(['id'=>$request->asset_id])->first();

    //     $data = array(
    //     	'post_title'=>$request->post_title?$request->post_title:$post['post_title'],
    //         'post_content'=>$request->post_content?$request->post_content:$post['post_content'],
    //         'post_status'=>'publish',
    //         'comment_status'=>'open',
    //         'post_excerpt'=>'NULL',
    //         'to_ping'=>'NULL',
    //         'pinged'=>'NULL',
    //         'post_content_filtered'=>'NULL',
    //         'post_name'=>$request->post_name?$request->post_name:$post['post_name'],
    //         'menu_order'=>$request->menu_order?$request->menu_order:$post['menu_order'],
    //     );

    //      Post::where('id', $request->asset_id)->update($data);

    //     return response()->json(['status' => 1,'message' => 'Updated Successfully.']);

    // }

    // public function getAssets(Request $request){
    // 	$validator = Validator::make($request->all(), [
    //         'user_id' => 'required'
    //     ]);
    //     if ($validator->fails()) {
    //         return response()->json(['message'=>$validator->errors()->first(),'status'=>'false'], $this->badrequest);
    //     }else{
    //     	$addArr = [];
    //         $addresses = Post::orderBy('id','ASC')
    //         ->where('post_author','=',$request->user_id)
    //         ->get();
    //         $larr = json_decode($addresses,true);
	   //      return response()->json([
	   //      	'status'=>'1',
    //             'message'=>'Assets fetched successfully',
    //             'assetsArray'=>$larr
    //         ], $this->successStatus);
    //     }
    // }

    // public function addAssetToTimeline(Request $request){

    //     $validator = Validator::make($request->all(), [
    //         'asset_id' => 'required',
    //         'title' => 'required',
    //         'description' => 'required',
    //         // 'status' => 'required',
    //         'expense' => 'required',
    //         'year' => 'required',
    //         'month' => 'required',
    //         'videourl' => 'required',
    //         'picture' => 'required'
    //     ]);

    //     if ($validator->fails()) {
    //         return response()->json(['status' => 0,'message'=>"All fields are required"]);
    //     }


    //     if ($request->hasFile('picture')) {

    //         if ($request->file('picture')->isValid()) {

    //         	$image_name = time().'.'.$request->picture->extension();

    //             $request->picture->move(public_path('timeline_images'), $image_name);

    //         }else{
    //             $image_name ="default.png";
    //         }

    //     }else{
    //         $image_name ="default.png";
    //     }


    //     $data = array(
    //         'asset_id'=>$request->asset_id,
    //     	'title'=>$request->title,
    //         'image'=>$image_name,
    //         'content'=>$request->description,
    //         'date'=>$request->month.$request->year,
    //         'amount'=>$request->expense,
    //         'videourl'=>$request->videourl
    //     );


    //     $posts = Timeline::create($data);
    //     $post = Timeline::where(['id'=>$posts->id])->first();


    //     return response()->json(['status' => 1,'message' => 'Addto Timeline Successfully.','result' =>$post]);

    // }
}
