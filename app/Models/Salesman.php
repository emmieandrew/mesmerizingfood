<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    public $table = 'users';
    protected $primaryKey = 'id';

    protected $fillable = [
        'image_name', 'title','email', 'country','dob','status','phone','city','monthly_target','type','password','device_token'
    ];
    // public function teamtype(){
    // 	return $this -> belongsTo('App\Models\Teamtype','type','id');
    // }

    // public function getall()
    // {
    // 	return $this->belongsTo('App\Models\OtherUsers');
    // }
    // public function branchdata()
    // {
    //     return $this->belongsTo('App\Models\Branch','branch','id');
    // }
    // public function salary()
    // {
    //     return $this->belongsTo('App\Models\Salary','emp_id','emp_id');
    // }
}
