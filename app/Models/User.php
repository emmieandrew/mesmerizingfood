<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class User
 * @package App\Models
 * @version April 25, 2020, 7:48 pm UTC
 *
 * @property string $device_id
 * @property string $device_type
 * @property string $remember_token
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $DOB
 * @property string $country
 * @property string $gender
 * @property string $phone_no
 * @property string $image
 * @property string $type
 * @property string $login_status
 * @property string|\Carbon\Carbon $email_verified_at
 */
class User extends Model
{
    use SoftDeletes;
    public $timestamps = false;

    public $table = 'users';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'country',
        'city',
        'dob',
        'type'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */

    protected $hidden = ['password','deleted_at','updated_at'];
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'password' => 'string',
        'phone' => 'string',
        'country' => 'string',
        'city' => 'string',
        'dob' => 'string',
        'type' => 'string'
    ];
    

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'email' => 'required',
    ];

    public function detail()
    {
        return $this->hasOne('App\Models\Detail');
    }

    // public function posts()
    // {
    //     return $this->hasOne('App\Detail');
    // }
    
}
