<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    public $timestamps = false;
    public $table = 'user_details';

    public $fillable = [
        'user_id',
        'specification',
        'tags',
        'languages',
        'experiences',
        'documents'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'string',
        'specification' => 'string',
        'tags' => 'string',
        'languages' => 'string',
        'experiences' => 'string',
        'documents' => 'string'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

}
