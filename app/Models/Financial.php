<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Financial extends Model
{
    
    use SoftDeletes;
    public $timestamps = false;
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];
    public $table = 'financials';

    public $fillable = [
        'ratio',
        'company_id',
        'tutor_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'ratio' => 'string',
        'company_id' => 'string',
        'tutor_id' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'ratio' => 'required',
        'company_id' => 'required',
        'tutor_id' => 'required',
    ];
}
