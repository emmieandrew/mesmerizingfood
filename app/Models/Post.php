<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //wp_posts
    public $timestamps = false;
    public $table = 'posts';

    public $fillable = [
        'user_id',
        'title',
        'description',
        'files'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'string',
        'title' => 'string',
        'description' => 'string',
        'files' => 'string'
    ];
}
