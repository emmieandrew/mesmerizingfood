-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 27, 2021 at 07:11 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.3.26-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `MESMERIZINGFood`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(155) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` varchar(155) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `long` varchar(155) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_token` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `remember_token`, `phone`, `address`, `lat`, `long`, `device_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', 'admin@mesmerizing.com', '$2y$10$sFESc6Ip56WsB4nUi3U0KuW.rcUxVyHLljD6uDrTdzRZBU8g7HazW', 'LXDPCr0JPYT7jadmbAtPrmJFCyIfNV7meKLsk5k2ls1EQSW0RekNU1036zzY', '012457896320', '2615 Abdul Mumin Ad Dimayati, Al Badiah, Riyadh 12748 6098, Saudi Arabia', '24.6137435', '46.6809948', NULL, '2018-12-18 01:49:22', '2020-12-16 04:49:40', NULL),
(2, 'admin', 'amit@admin.com', '$2y$10$sFESc6Ip56WsB4nUi3U0KuW.rcUxVyHLljD6uDrTdzRZBU8g7HazW', 'N2GBEqqcwFjnNvNxQdHZvHeGExu0dmrvIGvuQfFQS51MowDDMHUrw9b8hngD', '012457896320', '2615 Abdul Mumin Ad Dimayati, Al Badiah, Riyadh 12748 6098, Saudi Arabia', '24.6137435', '46.6809948', NULL, '2018-12-18 01:49:22', '2021-10-18 20:31:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Hospital.jpg',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `name`, `icon`, `created_at`, `updated_at`, `deleted_at`) VALUES
(16, 0, 'dummy', 'Hospital.jpg', '2021-10-24 06:41:19', NULL, NULL),
(17, 16, 'dummy sub', 'Hospital.jpg', '2021-10-24 06:41:31', NULL, NULL),
(18, 16, 'dummy sub 1', 'Hospital.jpg', '2021-10-24 06:41:36', NULL, NULL),
(19, 0, 'new cat', 'Hospital.jpg', '2021-10-24 06:41:49', NULL, NULL),
(20, 19, 'new cat 1', 'Hospital.jpg', '2021-10-24 06:41:54', NULL, NULL),
(21, 0, 'new catgory', 'Hospital.jpg', '2021-10-24 08:23:57', NULL, NULL),
(22, 0, 'new catgory', 'Hospital.jpg', '2021-10-26 12:44:37', NULL, NULL),
(23, 22, 'new catgory', 'Hospital.jpg', '2021-10-26 16:19:17', NULL, NULL),
(24, 12, 'new catgory', 'Hospital.jpg', '2021-10-26 16:25:58', NULL, NULL),
(25, 0, 'new catgory 123', 'Hospital.jpg', '2021-10-26 16:33:50', NULL, NULL),
(26, 25, 'new catgory 12345', 'Hospital.jpg', '2021-10-26 16:34:14', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contents`
--

CREATE TABLE `contents` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contents`
--

INSERT INTO `contents` (`id`, `title`, `content`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Terms And Conditions', '<p>Far Far Away, Behind The Word Mountains, Far From The Countries Vokalia And Consonantia, There Live The Blind Texts.</p><p>Separated They Live In Bookmarksgrove Right At The Coast Of The Semantics, A Large Language Ocean. A Small River Named Duden Flows By Their Place And Supplies It With The Necessary Regelialia.</p><p>It Is A Paradisematic Country, In Which Roasted Parts Of Sentences Fly Into Your Mouth.</p><p>Even The All-Powerful Pointing Has No Control About The Blind Texts It Is An Almost Unorthographic Life One Day However A Small Line Of Blind Text By The Name Of Lorem Ipsum Decided To Leave For The Far World Of Grammar.</p><p>The Big Oxmox Advised Her Not To Do So, Because There Were Thousands Of Bad Commas, Wild Question Marks And Devious Semikoli, But The Little Blind Text Didn&#8217;t Listen. She Packed Her Seven Versalia, Put Her Initial Into The Belt And Made Herself On The Way.</p><p>When She Reached The First Hills Of The Italic Mountains, She Had A Last View Back On The Skyline Of Her Hometown Bookmarksgrove, The Headline Of Alphabet Village And The Subline Of Her Own Road, The Line Lane. Pityful A Rethoric Question Ran Over Her Cheek, Then</p>', '2018-12-19 10:07:00', '2018-12-19 10:07:00', NULL),
(2, 'Privacy Policy', '<p>Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit. Aenean Commodo Ligula Eget Dolor. Aenean Massa. Cum Sociis Natoque Penatibus Et Magnis Dis Parturient Montes, Nascetur Ridiculus Mus. Donec Quam Felis, Ultricies Nec, Pellentesque Eu, Pretium Quis, Sem. Nulla Consequat Massa Quis Enim. Donec Pede Justo, Fringilla Vel, Aliquet Nec, Vulputate Eget, Arcu. In Enim Justo, Rhoncus Ut, Imperdiet A, Venenatis Vitae, Justo. Nullam Dictum Felis Eu Pede Mollis Pretium. Integer Tincidunt. Cras Dapibus. Vivamus Elementum Semper Nisi. Aenean Vulputate Eleifend Tellus. Aenean Leo Ligula, Porttitor Eu, Consequat Vitae, Eleifend Ac, Enim. Aliquam Lorem Ante, Dapibus In, Viverra Quis, Feugiat A, Tellus. Phasellus Viverra Nulla Ut Metus Varius Laoreet. Quisque Rutrum. Aenean Imperdiet. Etiam Ultricies Nisi Vel Augue. Curabitur Ullamcorper Ultricies Nisi. Nam Eget Dui. Etiam Rhoncus. Maecenas Tempus, Tellus Eget Condimentum Rhoncus, Sem Quam Semper Libero, Sit Amet Adipiscing Sem Neque Sed Ipsum. Nam Quam Nunc, Blandit Vel, Luctus Pulvinar, Hendrerit Id, Lorem. Maecenas Nec Odio Et Ante Tincidunt Tempus. Donec Vitae Sapien Ut Libero Venenatis Faucibus. Nullam Quis Ante. Etiam Sit Amet Orci Eget Eros Faucibus Tincidunt. Duis Leo. Sed Fringilla Mauris Sit Amet Nibh. Donec Sodales Sagittis Magna. Sed Consequat, Leo Eget Bibendum Sodales, Augue Velit Cursus Nunc,</p><h2>HOW IT WORKS</h2><h3>Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit. Aenean Commodo Ligula Eget Dolor. Aenean Massa. Cum Sociis Natoque Penatibus Et Magnis Dis Parturient Montes, Nascetur Ridiculus Mus. Donec Quam Felis, Ultricies Nec, Pellentesque Eu, Pretium Quis, Sem. Nulla Consequat Massa Quis Enim. Donec Pede Justo, Fringilla Vel, Aliquet Nec, Vulputate Eget, Arcu. In Enim Justo, Rhoncus Ut, Imperdiet A, Venenatis Vitae, Justo. Nullam Dictum Felis Eu Pede Mollis Pretium. Integer Tincidunt. Cras Dapibus. Vivamus Elementum Semper Nisi. Aenean Vulputate Eleifend Tellus. Aenean Leo Ligula, Porttitor Eu, Consequat Vitae, Eleifend Ac, Enim. Aliquam Lorem Ante, Dapibus In, Viverra Quis, Feugiat A, Tellus. Phasellus Viverra Nulla Ut Metus Varius Laoreet. Quisque Rutrum. Aenean Imperdiet. Etiam Ultricies Nisi Vel Augue. Curabitur Ullamcorper Ultricies Nisi. Nam Eget Dui. Etiam Rhoncus. Maecenas Tempus, Tellus Eget Condimentum Rhoncus, Sem Quam Semper Libero, Sit Amet Adipiscing Sem Neque Sed Ipsum. Nam Quam Nunc, Blandit Vel, Luctus Pulvinar, Hendrerit Id, Lorem. Maecenas Nec Odio Et Ante Tincidunt Tempus. Donec Vitae Sapien Ut Libero Venenatis Faucibus. Nullam Quis Ante. Etiam Sit Amet Orci Eget Eros Faucibus Tincidunt. Duis Leo. Sed Fringilla Mauris Sit Amet Nibh. Donec Sodales Sagittis Magna. Sed Consequat, Leo Eget Bibendum Sodales, Augue Velit Cursus Nunc,</h3>', '2018-12-19 10:11:49', '2018-12-19 10:11:49', NULL),
(3, 'About US', '<p>Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit. Aenean Commodo Ligula Eget Dolor. Aenean Massa. Cum Sociis Natoque Penatibus Et Magnis Dis Parturient Montes, Nascetur Ridiculus Mus. Donec Quam Felis, Ultricies Nec, Pellentesque Eu, Pretium Quis, Sem. Nulla Consequat Massa Quis Enim. Donec Pede Justo, Fringilla Vel, Aliquet Nec, Vulputate Eget, Arcu. In Enim Justo, Rhoncus Ut, Imperdiet A, Venenatis Vitae, Justo. Nullam Dictum Felis Eu Pede Mollis Pretium. Integer Tincidunt. Cras Dapibus. Vivamus Elementum Semper Nisi. Aenean Vulputate Eleifend Tellus. Aenean Leo Ligula, Porttitor Eu, Consequat Vitae, Eleifend Ac, Enim. Aliquam Lorem Ante, Dapibus In, Viverra Quis, Feugiat A, Tellus. Phasellus Viverra Nulla Ut Metus Varius Laoreet. Quisque Rutrum. Aenean Imperdiet. Etiam Ultricies Nisi Vel Augue. Curabitur Ullamcorper Ultricies Nisi. Nam Eget Dui. Etiam Rhoncus. Maecenas Tempus, Tellus Eget Condimentum Rhoncus, Sem Quam Semper Libero, Sit Amet Adipiscing Sem Neque Sed Ipsum. Nam Quam Nunc, Blandit Vel, Luctus Pulvinar, Hendrerit Id, Lorem. Maecenas Nec Odio Et Ante Tincidunt Tempus. Donec Vitae Sapien Ut Libero Venenatis Faucibus. Nullam Quis Ante. Etiam Sit Amet Orci Eget Eros Faucibus Tincidunt. Duis Leo. Sed Fringilla Mauris Sit Amet Nibh. Donec Sodales Sagittis Magna. Sed Consequat, Leo Eget Bibendum Sodales, Augue Velit Cursus Nunc,</p><h2>HOW IT WORKS</h2><h3>Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit. Aenean Commodo Ligula Eget Dolor. Aenean Massa. Cum Sociis Natoque Penatibus Et Magnis Dis Parturient Montes, Nascetur Ridiculus Mus. Donec Quam Felis, Ultricies Nec, Pellentesque Eu, Pretium Quis, Sem. Nulla Consequat Massa Quis Enim. Donec Pede Justo, Fringilla Vel, Aliquet Nec, Vulputate Eget, Arcu. In Enim Justo, Rhoncus Ut, Imperdiet A, Venenatis Vitae, Justo. Nullam Dictum Felis Eu Pede Mollis Pretium. Integer Tincidunt. Cras Dapibus. Vivamus Elementum Semper Nisi. Aenean Vulputate Eleifend Tellus. Aenean Leo Ligula, Porttitor Eu, Consequat Vitae, Eleifend Ac, Enim. Aliquam Lorem Ante, Dapibus In, Viverra Quis, Feugiat A, Tellus. Phasellus Viverra Nulla Ut Metus Varius Laoreet. Quisque Rutrum. Aenean Imperdiet. Etiam Ultricies Nisi Vel Augue. Curabitur Ullamcorper Ultricies Nisi. Nam Eget Dui. Etiam Rhoncus. Maecenas Tempus, Tellus Eget Condimentum Rhoncus, Sem Quam Semper Libero, Sit Amet Adipiscing Sem Neque Sed Ipsum. Nam Quam Nunc, Blandit Vel, Luctus Pulvinar, Hendrerit Id, Lorem. Maecenas Nec Odio Et Ante Tincidunt Tempus. Donec Vitae Sapien Ut Libero Venenatis Faucibus. Nullam Quis Ante. Etiam Sit Amet Orci Eget Eros Faucibus Tincidunt. Duis Leo. Sed Fringilla Mauris Sit Amet Nibh. Donec Sodales Sagittis Magna. Sed Consequat, Leo Eget Bibendum Sodales, Augue Velit Cursus Nunc,</h3>', '2018-12-19 10:12:02', '2018-12-19 10:12:02', NULL),
(4, 'Features', '<p>Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test</p>', '2019-01-18 11:07:39', '2019-01-18 11:07:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(10) UNSIGNED NOT NULL,
  `question` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL,
  `faq_categories_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `question`, `answer`, `status`, `sort_order`, `faq_categories_id`, `created_at`, `updated_at`) VALUES
(6, 'Test Test Test', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,</p>', '1', 1, 4, '2019-01-31 07:54:52', '2019-07-12 09:43:35');

-- --------------------------------------------------------

--
-- Table structure for table `financials`
--

CREATE TABLE `financials` (
  `id` int(11) NOT NULL,
  `ratio` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` int(11) NOT NULL,
  `tutor_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `financials`
--

INSERT INTO `financials` (`id`, `ratio`, `company_id`, `tutor_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '20', 1, 2, '2021-10-24 07:50:41', NULL, NULL),
(2, '30', 1, 3, '2021-10-24 07:57:22', NULL, NULL),
(3, '83', 1, 84, '2021-10-25 17:48:41', NULL, NULL),
(4, '26', 1, 88, '2021-10-25 17:50:39', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','inactive','deleted') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'test', 'active', '2021-10-24 07:21:55', NULL, NULL),
(2, 'test 2', 'active', '2021-10-24 07:22:02', NULL, NULL),
(3, 'test fdfd', 'active', '2021-10-24 07:21:55', NULL, NULL),
(4, 'test dcvcb cvc', 'active', '2021-10-24 07:22:02', NULL, NULL),
(5, 'wdsadcs', 'active', '2021-10-24 20:54:13', NULL, NULL),
(6, 'test', 'active', '2021-10-24 20:54:31', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `role_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions_ids` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','deactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role_name`, `permissions_ids`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', '1,2', 'active', '2021-10-24 07:22:54', NULL, NULL),
(2, 'User', '1', 'active', '2021-10-24 08:11:42', NULL, NULL),
(3, 'testc', '1', 'active', '2021-10-24 19:13:09', NULL, NULL),
(4, 'type', '3', 'active', '2021-10-24 19:15:06', NULL, NULL),
(5, 'retg re', '1', 'active', '2021-10-24 19:15:24', NULL, NULL),
(6, 'testing', '2', 'active', '2021-10-24 19:29:17', NULL, NULL),
(7, 'testing', '1', 'active', '2021-10-24 19:29:17', NULL, NULL),
(8, 'testing', '4', 'active', '2021-10-24 19:29:17', NULL, NULL),
(9, 'testing', '3', 'active', '2021-10-24 19:29:17', NULL, NULL),
(10, 'dfsdff', '1,2,3,4,5,6', 'active', '2021-10-24 21:00:30', NULL, NULL),
(11, 'Harman', '1,2,3', 'active', '2021-10-26 16:50:44', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `type` enum('1','2') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '1-totor, 2-learner',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '1=>active,0=>deactivate',
  `phone` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` enum('male','female','other') COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otp` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'default.jpg',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `type`, `name`, `email`, `dob`, `password`, `description`, `status`, `phone`, `country`, `city`, `gender`, `state`, `otp`, `image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1', '2', 'Admin', 'admin@mesmerizing.com', '11-01-1122', '$2y$10$cmrZus1Mt5pannvtXv8ZVeVQJtbLDk.idcpkEbzYkvKDln/uTyjz2', NULL, '1', '32232323', 'sdsd', 'dsds', 'male', NULL, NULL, 'default.jpg', '2021-10-24 05:53:06', NULL, NULL),
(2, '1', '2', 'nishant', 'amiiiididt@ff.com', '11-01-1122', '$2y$10$l0.YVqvXOTavjjmD/1NUS.xpEG7MSVgdisE8KJJRwzdE8QA64dZ.u', NULL, '1', '32232323', 'sdsd', 'dsds', 'male', NULL, NULL, 'default.jpg', '2021-10-24 05:40:56', NULL, NULL),
(3, '1', '1', 'dsdsds', 'amiiiiit@ff.com', '11-01-1122', '$2y$10$mbGJJoQrBYJemJRgRuEHAe8ETK8b4sRQO7d9V98YM0WVmY6bn4viO', NULL, '1', '32232323', 'sdsd', 'dsds', 'male', NULL, NULL, 'default.jpg', '2021-10-24 05:40:08', NULL, NULL),
(82, '1', '2', 'dsdsds', 'demo@gmail.com', '11-01-1122', '$2y$10$gUVpw4cmVTxfA36MEJbQoulDriQoNqNcxfOXDyR66p/KdClX/rmPC', NULL, '1', '32232323', 'sdsd', 'dsds', 'male', NULL, NULL, 'default.jpg', '2021-10-24 16:34:21', NULL, NULL),
(83, '1', '2', 'gdhd', 'dfgfdg@hfg.com', '2222-03-23', '$2y$10$ftvUTQur8WrBG1oGiYlwPOf7WatkV0DhDCRrP6Fd2aK/rU9TsQb.m', NULL, '1', '45435', 'fsdf', 'fgdfgdfsg', 'male', NULL, NULL, 'default.jpg', '2021-10-25 04:21:06', NULL, NULL),
(84, '1', '1', 'test', 'tessdf@fgdf.com', '2014-02-23', '$2y$10$pT1atc.cpXsVBdW4TFUwt.1LTJ0gY49VHbhYySKJe3eetRmL/cK.S', NULL, '1', '5345345', 'cxvzv', 'dfgdfgdfsg', 'male', NULL, NULL, 'default.jpg', '2021-10-25 04:22:24', NULL, NULL),
(85, '1', '2', 'fdgds', 'sdfsdf@gh.com', '2013-02-12', '$2y$10$oV.5Z.rE9hJBHLG1Rdw4vOgeUocvjOgNJi.jds73/jS9.hS9ph50G', NULL, '1', '654656', 'sdcvxc', 'gfhfgvnb', 'male', NULL, NULL, 'default.jpg', '2021-10-25 04:23:50', NULL, NULL),
(86, '1', '2', 'dsdsds', 'demo1234@gmail.com', '11-01-1122', '$2y$10$POlXG.YVoSY8F/Cc7HlmNei79j4C72cFXfuDsoVx9LUNI0TacBB.C', NULL, '1', '32232323', 'sdsd', 'dsds', 'male', NULL, NULL, 'default.jpg', '2021-10-25 04:26:11', NULL, NULL),
(87, '1', '2', 'fdgds', 'sdfsdxzf@gh.com', '2013-02-12', '$2y$10$dJYAtkzeKo8ZM9UXrZfQCO5lfFhj5oPImexfcxoVR1E00cI57IiCi', NULL, '1', '654656', 'sdcvxc', 'gfhfgvnb', 'male', NULL, NULL, 'default.jpg', '2021-10-25 04:28:02', NULL, NULL),
(88, '1', '1', 'Test', 'test1212@gmail.com', '1981-10-25', '$2y$10$GFAdGWAuQ4VnBt2TDWraEO7VO4OYDKOjm6GhIuuJPSFremd15aa02', NULL, '1', '9878798765', 'India', 'Mohali', 'male', NULL, NULL, 'default.jpg', '2021-10-25 11:19:22', NULL, NULL),
(89, '1', '2', 'Demo 2', 'demo2@gmail.com', '11-01-1122', '$2y$10$6aoRSnDN5XM8CJrUTgF09etPD1rDNo4Kj9ENjUx.qS5GhN6hT7WWO', NULL, '1', '32232323', 'sdsd', 'dsds', 'male', NULL, NULL, 'default.jpg', '2021-10-26 11:19:28', NULL, NULL),
(90, '1', '2', 'KL Rahul', 'klrahul@yopmail.com', '26 Oct 2011', '$2y$10$iZLlNZFGdcbRfOIbiNZaweMKGIGxJs/Ed7eWKg42XqzOel1zx92fG', NULL, '1', '+91976766444646', 'dnxjjd', 'mdmek', 'male', NULL, NULL, 'default.jpg', '2021-10-26 11:46:24', NULL, NULL),
(91, '1', '2', 'KL Rahul', 'klrahul2@yopmail.com', '26 Oct 2011', '$2y$10$xYFDHTcuOpfraBuKXhrXWuT6ktStY9XA.9IUrpsC5.rN1HnZvTIW6', NULL, '1', '+91976766444646', 'dnxjjd', 'mdmek', 'male', NULL, NULL, 'default.jpg', '2021-10-26 11:48:14', NULL, NULL),
(92, '1', '2', 'KL Rahul three', 'klrahul3@yopmail.com', '26 Oct 2011', '$2y$10$0NmE.Ti2hOG2KluHzrS42e5fxv7jVYRXeC3i/4P8fgtdabPf2kzjy', NULL, '1', '+91976766444646', 'dnxjjd', 'mdmek', 'male', NULL, NULL, 'default.jpg', '2021-10-26 11:49:01', NULL, NULL),
(93, '1', '2', 'Eeememme', 'nssnsm@nzmmsms.dmmd', '26 Oct 2011', '$2y$10$sQk0kKcSh0.6.6ezVnyIveKiSsgfpKxTdFzDzLHHEvp/2JwIK4xOi', NULL, '1', '+91949114949', 'nsjms', 'emmemee', 'male', NULL, NULL, 'default.jpg', '2021-10-26 11:53:25', NULL, NULL),
(94, '1', '2', 'Snssnsjs', 'nsns@nmzz.nzmz', '26 Oct 2011', '$2y$10$GMUNDY1trxg6bMQY0YYHd.sukJZzt.N7OdhtUS4jAAAQ1zvDlLbsW', NULL, '1', '+919797767674', 'nznzs', 'nssnd', 'male', NULL, NULL, 'default.jpg', '2021-10-26 11:54:59', NULL, NULL),
(95, '1', '1', 'Admin Demo', 'adminDemo@gmail.com', '11-01-1122', '$2y$10$adW.LSxI5T7wQAGE659fY.RtLYrU8GssIZRxs24ngv3Zm2DRcFkvW', NULL, '1', '32232323', 'sdsd', 'dsds', 'male', NULL, NULL, 'default.jpg', '2021-10-26 13:01:15', NULL, NULL),
(96, '1', '1', 'Ravinder Admin', 'ravinder@yopmail.com', '27 Oct 2011', '$2y$10$PdyOCekGYT2pUu0YLRardu1qRMrNcMmdkgfgaUzfen.lMOGjW6oe2', NULL, '1', '+91979797979', 'India', 'jaaja', 'male', NULL, NULL, 'default.jpg', '2021-10-27 05:42:22', NULL, NULL),
(97, '1', '2', 'dsdsds', 'demo1@gmail.com', '11-01-1122', '$2y$10$/4NQxvt3Grvs9rvgRbSzRuKIlFyQQWkaRThq1cq0FqK57ONtEgn/q', NULL, '1', '32232323', 'sdsd', 'dsds', 'male', NULL, '4661', 'default.jpg', '2021-10-27 07:00:39', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contents`
--
ALTER TABLE `contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `financials`
--
ALTER TABLE `financials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `contents`
--
ALTER TABLE `contents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `financials`
--
ALTER TABLE `financials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
